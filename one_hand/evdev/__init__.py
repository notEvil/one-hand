import evdev.ecodes as e_ecodes
import ctypes


KEY_UP = 0
KEY_DOWN = 1
KEY_HOLD = 2

SHIFT_MASK = 1 << 0
RIGHT_ALT_MASK = 1 << 7
CONTROL_MASK = 1 << 2
LEFT_ALT_MASK = 1 << 3


class X11:
    def __init__(self):
        self._libX11 = libX11 = ctypes.cdll.LoadLibrary("libX11.so")

        libX11.XOpenDisplay.restype = ctypes.POINTER(_Display)

        self._display = libX11.XOpenDisplay(None)

    def get_string_from_key_code(self, key_code, modifier_masks=None):
        assert self._display is not None

        if modifier_masks is None:
            modifier_masks = []

        state = 0
        for modifier_mask in modifier_masks:
            state |= modifier_mask

        event_struct = _XKeyEvent(
            keycode=type(self).get_x11_keycode_from_evdev_keycode(key_code),
            state=state,
            display=self._display,
        )
        bytes_buffer = 4
        buffer_return = (ctypes.c_char * bytes_buffer)()
        keysym_return = None
        status_in_out = None

        buffer_length = self._libX11.XLookupString(
            ctypes.pointer(event_struct),
            ctypes.pointer(buffer_return),
            bytes_buffer,
            keysym_return,
            status_in_out,
        )

        result = buffer_return.value[:buffer_length].decode()
        return result

    def get_strings_by_key_codes(self, modifier_masks=None):
        items = (
            (
                key_code,
                self.get_string_from_key_code(key_code, modifier_masks=modifier_masks),
            )
            for key_code in e_ecodes.keys.keys()
        )
        result = {key_code: string for key_code, string in items if len(string) != 0}
        return result

    @staticmethod
    def get_x11_keycode_from_evdev_keycode(keycode):
        result = keycode + 8
        return result

    def close(self):
        if self._display is None:
            return

        self._libX11.XCloseDisplay(self._display)
        self._display = None

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()


# https://stackoverflow.com/questions/29638210/how-can-i-use-python-xlib-to-generate-a-single-keypress
class _Display(ctypes.Structure):
    """ opaque struct """


class _XKeyEvent(ctypes.Structure):
    _fields_ = [
        ("type", ctypes.c_int),
        ("serial", ctypes.c_ulong),
        ("send_event", ctypes.c_int),
        ("display", ctypes.POINTER(_Display)),
        ("window", ctypes.c_ulong),
        ("root", ctypes.c_ulong),
        ("subwindow", ctypes.c_ulong),
        ("time", ctypes.c_ulong),
        ("x", ctypes.c_int),
        ("y", ctypes.c_int),
        ("x_root", ctypes.c_int),
        ("y_root", ctypes.c_int),
        ("state", ctypes.c_uint),
        ("keycode", ctypes.c_uint),
        ("same_screen", ctypes.c_int),
    ]


def write_key_event(key_code, state, u_input):
    u_input.write(e_ecodes.EV_KEY, key_code, state)


def write_key(key, key_code_by_modifier_mask, u_input):
    modifier_masks, key_code = key

    for modifier_mask in modifier_masks:
        write_key_event(key_code_by_modifier_mask[modifier_mask], KEY_DOWN, u_input)

    write_key_event(key_code, KEY_DOWN, u_input)
    write_key_event(key_code, KEY_UP, u_input)

    for modifier_mask in modifier_masks:
        write_key_event(key_code_by_modifier_mask[modifier_mask], KEY_UP, u_input)


def write_string(string, key_code_by_modifier_mask, key_by_string, u_input):
    strings = split_string(string, key_by_string.keys())

    for string in strings:
        key = key_by_string[string]

        write_key(key, key_code_by_modifier_mask, u_input)


def delete_characters(number, u_input):
    for _ in range(number):
        write_key_event(e_ecodes.KEY_BACKSPACE, KEY_DOWN, u_input)
        write_key_event(e_ecodes.KEY_BACKSPACE, KEY_UP, u_input)


# TODO general function
def split_string(string, parts):
    result = []

    if not _split_string(0, result, string, parts):
        return None

    return result


def _split_string(start_index, result, string, parts):
    if len(string) <= start_index:
        return True

    for stop_index in range(start_index + 1, len(string) + 1):
        part = string[start_index:stop_index]

        if part not in parts:
            continue

        result.append(part)

        if _split_string(stop_index, result, string, parts):
            return True

        result.pop()

    return False
