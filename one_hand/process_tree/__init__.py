class Node:
    def __init__(self):
        self._processing = False
        self._processed_version = None

    def process(self, version, *args, **kwargs):
        if version == self._processed_version or self._processing:
            return

        self._processing = True

        result = self._process(version, *args, **kwargs)

        self._processed_version = version
        self._processing = False

        return result

    def _process(self, version, *args, **kwargs):
        raise NotImplementedError


class ReturnValue:
    def __init__(self, exit=None, stop=None):
        self.exit = exit
        self.stop = stop


def is_exit_or_stop(return_value):
    if return_value is None:
        return False

    if return_value.exit is True or return_value.stop is True:
        return True

    return False


def process(version, nodes, *args, **kwargs):
    for node in nodes:
        return_value = node.process(version, *args, **kwargs)

        if is_exit_or_stop(return_value):
            return return_value

    return None
