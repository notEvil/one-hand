import one_hand.evdev as o_evdev
import one_hand.weakref2 as o_weakref
import evdev.ecodes as e_ecodes
import plyvel
import symspellpy
import collections
import collections.abc as c_abc
import inspect
import logging
import os.path as o_path
import tempfile


class NOT_SET:
    pass


def get_short_path(path, get_short_name=None):
    if get_short_name is None:
        get_short_name = _get_short_name

    names = path.split("/")
    result = "/".join(
        get_short_name(name, len(names) - index - 1) for index, name in enumerate(names)
    )
    return result


def _get_short_name(name, depth):
    if depth == 0:
        result = name
        return result

    if len(name) == 0:
        result = ""
        return result

    result = name[0]
    return result


def get_source_location(depth=0):
    frame = inspect.currentframe()
    for _ in range(depth + 1):
        frame = frame.f_back

    frame_info = inspect.getframeinfo(frame)
    result = (frame_info.filename, frame_info.lineno)
    return result


def format_source_location(depth=0, get_short_name=NOT_SET):
    file_path, line_number = get_source_location(depth=depth + 1)

    if get_short_name is not NOT_SET:
        file_path = get_short_path(file_path, get_short_name=get_short_name)

    result = "{}:{}".format(file_path, line_number)
    return result


def print_source_location(depth=0, get_short_name=NOT_SET):
    print(format_source_location(depth=depth + 1, get_short_name=get_short_name))


def enumerate_reversed(x):
    result = zip(range(len(x) - 1, -1, -1), x)
    return result


# TODO replace by full fledged implementation
class PlyvelDict(collections.MutableMapping):
    def __init__(self, path, auto_collect=float("inf")):
        self.path = path
        self.auto_collect = auto_collect

        self._db = plyvel.DB(path, create_if_missing=True)

        self._cache = o_weakref.WeakValueDictionary()
        self._changes_count = 0

    def __setitem__(self, key, value):
        self._write(key, value)

        if isinstance(value, c_abc.Hashable):
            self._cache.pop(key, default=None)

        else:
            self._cache.set_item(key, value, self._collect_value, tag=key)

            self._changes_count += 1
            if self.auto_collect <= self._changes_count:
                self.collect()

    def _write(self, key, value):
        leveldb_key = repr(key).encode()
        leveldb_value = repr(value).encode()
        self._db.put(leveldb_key, leveldb_value)

    def __getitem__(self, key):
        result = self._cache.get(key, NOT_SET)
        if result is not NOT_SET:
            return result

        leveldb_key = repr(key).encode()
        leveldb_value = self._db.get(leveldb_key, default=NOT_SET)

        if leveldb_value is NOT_SET:
            raise KeyError(key)

        result = eval(leveldb_value)

        if isinstance(result, c_abc.Hashable):
            self._cache.pop(key, default=None)

        else:
            self._cache.set_item(key, result, self._collect_value, tag=key)

            self._changes_count += 1
            if self.auto_collect <= self._changes_count:
                self.collect()

        return result

    def _collect_value(self, weak_reference):
        self._write(weak_reference.tag, weak_reference.object)

    def __iter__(self):
        for leveldb_key in self._db.iterator(include_value=False):
            key = eval(leveldb_key)
            yield key

    def __len__(self):
        raise NotImplementedError

    def __delitem__(self, key):
        raise NotImplementedError

    def close(self):
        self.collect()
        self._db.close()

    def __del__(self):
        self.close()

    def collect(self):
        logging.info(
            "collecting {} values @ {}".format(
                len(self._cache), format_source_location(get_short_name=None)
            )
        )

        result = self._cache.collect()

        self._changes_count = 0

        return result


class PlyvelDefaultDict(PlyvelDict):
    def __init__(self, default_factory, *args, **kwargs):
        self.default_factory = default_factory

        super().__init__(*args, **kwargs)

    def __getitem__(self, key):
        try:
            result = super().__getitem__(key)

        except KeyError:
            if self.default_factory is None:
                raise

            result = self.default_factory()
            self[key] = result

        return result


def get_character_mapping_from_x11(x11):
    result = {
        x11.get_string_from_key_code(
            key_key_code, modifier_masks=shift
        ): x11.get_string_from_key_code(value_key_code, modifier_masks=shift)
        for key_key_code, value_key_code in _key_mapping.items()
        for shift in [set(), {o_evdev.SHIFT_MASK}]
    }
    return result


_key_mapping = {
    e_ecodes.KEY_P: e_ecodes.KEY_Q,
    e_ecodes.KEY_O: e_ecodes.KEY_W,
    e_ecodes.KEY_I: e_ecodes.KEY_E,
    e_ecodes.KEY_U: e_ecodes.KEY_R,
    e_ecodes.KEY_Y: e_ecodes.KEY_T,
    e_ecodes.KEY_SEMICOLON: e_ecodes.KEY_A,
    e_ecodes.KEY_L: e_ecodes.KEY_S,
    e_ecodes.KEY_K: e_ecodes.KEY_D,
    e_ecodes.KEY_J: e_ecodes.KEY_F,
    e_ecodes.KEY_H: e_ecodes.KEY_G,
    e_ecodes.KEY_M: e_ecodes.KEY_C,
    e_ecodes.KEY_N: e_ecodes.KEY_V,
}


def prepare_symspell_dictionary(
    dictionary_path, character_mappings, target_path, target_word_mapping
):
    with tempfile.TemporaryDirectory() as temp_directory, open(
        dictionary_path, "r"
    ) as file, open(target_path, "w") as target_file:
        counts_path = o_path.join(temp_directory, "counts")
        assert not o_path.isdir(counts_path)
        counts = PlyvelDict(counts_path)

        for line in file:
            index = line.rfind(" ")
            word = line[:index]
            count = int(line[(index + 1) :])

            for character_mapping in character_mappings:
                new_word = "".join(
                    character_mapping.get(character, character) for character in word
                )

                counts[new_word] = max(counts.get(new_word, 0), count)

                target_word_mapping.setdefault(new_word, []).append(word)

        for new_word, count in counts.items():
            print(new_word, " ", count, sep="", file=target_file)

        counts.close()


class Suggester:
    def __init__(
        self,
        dictionary_path,
        word_mapping,
        max_edit_distance_dictionary=4,
        prefix_length=7,
        words_dict=None,
        deletes_defaultdict=None,
    ):
        self.dictionary_path = dictionary_path
        self.word_mapping = word_mapping
        self.max_edit_distance_dictionary = max_edit_distance_dictionary
        self.prefix_length = prefix_length

        self._sym_spell = sym_spell = symspellpy.SymSpell(
            max_edit_distance_dictionary,
            prefix_length,
            words_dict=words_dict,
            deletes_defaultdict=deletes_defaultdict,
        )

        result = sym_spell.load_dictionary(dictionary_path, 0, 1)
        assert result

    def get_suggestions(self, input):
        top_suggestions = self._sym_spell.lookup(input, symspellpy.Verbosity.TOP)

        for suggestion in top_suggestions:
            yield from self.word_mapping[suggestion.term]

        closest_suggestions = self._sym_spell.lookup(
            input, symspellpy.Verbosity.CLOSEST
        )

        for suggestion in closest_suggestions[len(top_suggestions) :]:
            yield from self.word_mapping[suggestion.term]

        all_suggestions = self._sym_spell.lookup(input, symspellpy.Verbosity.ALL)

        for suggestion in all_suggestions[len(closest_suggestions) :]:
            yield from self.word_mapping[suggestion.term]


class LazySequence(collections.Sequence):
    def __init__(self, iterable):
        self.iterable = iterable

        self._iterator = iter(iterable)
        self._list = []

    def __getitem__(self, index):
        if index < 0:
            raise IndexError(index)

        while len(self._list) <= index:
            try:
                next_ = next(self._iterator)

            except StopIteration:
                raise IndexError(index)

            self._list.append(next_)

        result = self._list[index]
        return result

    def __len__(self):
        raise NotImplementedError


if False:

    def read_hunspell_dictionary(path):
        with open(path, "r") as file:
            lines = iter(file)

            next(lines)  # skip first line

            for line in lines:
                line = line.strip()

                i = line.rfind("/")
                word = line[:i]
                yield word

    for word in read_hunspell_dictionary("./hunspell-en_US-2018.04.16/en_US.dic"):
        print(word)
