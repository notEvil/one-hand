import one_hand
import collections.abc as c_abc
import sys
import weakref


class COLLECTED:
    pass


class WeakReference:
    __slots__ = ("object", "callback", "tag", "_hash")

    _weak_references_by_object_id = {}  # {id(object): [WeakReference]}

    def __init__(self, object, callback, tag=None):
        self.object = object
        self.callback = callback
        self.tag = tag

        self._hash = None

        weak_references = type(self)._weak_references_by_object_id.get(id(object), None)

        if weak_references is None:
            type(self)._weak_references_by_object_id[id(object)] = [self]

        else:
            weak_references.append(self)

    def collect(self):
        if self.object is COLLECTED:
            return True

        weak_references = type(self)._weak_references_by_object_id[id(self.object)]

        filtered_references = _lazy_filter(
            weak_references, type(self)._filter_weak_reference
        )
        if filtered_references is not weak_references:
            type(self)._weak_references_by_object_id[
                id(self.object)
            ] = weak_references = filtered_references

        if sys.getrefcount(self.object) != len(weak_references) + 1:  # still referenced
            return False

        del type(self)._weak_references_by_object_id[id(self.object)]

        for weak_reference in weak_references:
            weak_reference.callback(weak_reference)
            weak_reference.object = COLLECTED

        return True

    @staticmethod
    def _filter_weak_reference(weak_reference):
        result = (
            sys.getrefcount(weak_reference) != 5
        )  # still referenced; in global collection, 2 from _lazy_filter, here, getrefcount
        return result

    def unregister(self):
        """
        - safe to call multiple times, unlike `register`
        """
        weak_references = type(self)._weak_references_by_object_id[id(self.object)]

        _right_remove_1(weak_references, self)

        if len(weak_references) == 0:
            del type(self)._weak_references_by_object_id[id(self.object)]

    def __hash__(self):
        result = self._hash
        if result is not None:
            return result

        if self.object is COLLECTED:
            raise Exception("object has been collected already")

        result = hash(self.object)
        self._hash = result
        return result

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            result = False
            return result

        if self.object is COLLECTED:
            raise Exception("object has been collected already")

        if other.object is COLLECTED:
            raise Exception("object has been collected already")

        result = other.object == self.object
        return result


def _lazy_filter(x, filter):
    """
    consistently increases reference count by 2
    """
    items = iter(x)

    index = 0

    for item in items:
        if not filter(item):
            break

        index += 1

    else:
        result = x
        return result

    result = x[:index]

    for item in items:
        if not filter(item):
            continue

        result.append(item)

    return result


def test_lazy_filter():
    class Class:
        def __init__(self, value):
            self.value = value

    objects = [Class(True), Class(False), Class(True)]

    def filter(object):
        assert (
            sys.getrefcount(object) == 5
        )  # in list, 2 from _lazy_filter, here, getrefcount
        return object.value

    result = _lazy_filter(objects, filter)

    assert result == [object for object in objects if object.value]


def _right_remove_1(x, item):
    for index, x_item in one_hand.enumerate_reversed(x):
        if x_item == item:
            break

    else:
        return

    del x[index : (index + 1)]


class WeakSet(c_abc.MutableSet):
    def __init__(self, items=None):
        super().__init__()

        self._set = set()

        if items is not None:
            self.update(items)

    def __contains__(self, item):
        weak_reference = WeakReference(item, None)
        result = weak_reference in self._set
        weak_reference.unregister()
        return result

    def __iter__(self):
        result = (
            weak_reference.object
            for weak_reference in self._set
            if weak_reference.object is not COLLECTED
        )
        return result

    def __len__(self):
        result = len(self._set)
        return result

    def add(self, item, callback=None, tag=None):
        weak_reference = WeakReference(
            item, self._pass_1 if callback is None else callback, tag=tag
        )
        length = len(self._set)

        self._set.add(weak_reference)

        if len(self._set) == length:
            weak_reference.unregister()

    def _pass_1(self, _):
        pass

    def discard(self, item):
        weak_reference = WeakReference(item, None)

        self._set.discard(weak_reference)

        if not weak_reference.collect():
            weak_reference.unregister()

    def collect(self):
        collected_references = set()

        for weak_reference in self._set:
            if weak_reference.collect():
                collected_references.add(weak_reference)

        self._set.difference_update(collected_references)


class WeakKeyDictionary(c_abc.MutableMapping):
    def __init__(self, items=None):
        super().__init__()

        self._dict = {}

        if items is not None:
            self.update(items)

    def __getitem__(self, key):
        weak_reference = WeakReference(key, None)

        try:
            result = self._dict[weak_reference]

        except KeyError:
            weak_reference.unregister()
            raise

        weak_reference.unregister()
        return result

    def __setitem__(self, key, value):
        self.set_item(key, value, self._pass_1)

    def _pass_1(self, _):
        pass

    def set_item(self, key, value, callback, tag=None):
        weak_reference = WeakReference(key, callback, tag=tag)
        length = len(self._dict)

        self._dict[weak_reference] = value

        if len(self._dict) == length:
            weak_reference.unregister()

    def __delitem__(self, key):
        weak_reference = WeakReference(key, None)

        del self._dict[weak_reference]

        if not weak_reference.collect():
            weak_reference.unregister()

    def __iter__(self):
        result = (
            weak_reference.object
            for weak_reference in self._dict.keys()
            if weak_reference.object is not COLLECTED
        )
        return result

    def __len__(self):
        result = len(self._dict)
        return result

    def collect(self):
        collected_references = []

        for weak_reference in self._dict.keys():
            if weak_reference.collect():
                collected_references.append(weak_reference)

        for weak_reference in collected_references:
            del self._dict[weak_reference]


class WeakValueDictionary(c_abc.MutableMapping):
    def __init__(self, items=None):
        super().__init__()

        self._dict = {}

        if items is not None:
            self.update(items)

    def __getitem__(self, key):
        result = self._dict[key].object

        if result is COLLECTED:
            raise KeyError(key)

        return result

    def __setitem__(self, key, value):
        self.set_item(key, value, self._pass_1)

    def _pass_1(self, _):
        pass

    def set_item(self, key, value, callback, tag=None):
        weak_reference = self._dict.get(key, None)
        if weak_reference is not None:
            if not weak_reference.collect():
                weak_reference.unregister()

        self._dict[key] = WeakReference(value, callback, tag=tag)

    def __delitem__(self, key):
        weak_reference = self._dict.pop(key)

        if not weak_reference.collect():
            weak_reference.unregister()

    def __iter__(self):
        result = (
            key
            for key, weak_reference in self._dict.items()
            if weak_reference.object is not COLLECTED
        )
        return result

    def __len__(self):
        result = len(self._dict)
        return result

    def collect(self):
        collected_keys = []

        for key, weak_reference in self._dict.items():
            if weak_reference.collect():
                collected_keys.append(key)

        for key in collected_keys:
            del self._dict[key]
