import collections.abc as c_abc
import sys
import weakref


class COLLECTED:
    pass


class WeakReference:
    _weak_references_by_object_id = {}  # {id(object): _WeakSet(WeakReference)}

    def __init__(self, object, callback, tag=None):
        self.object = object
        self.callback = callback
        self.tag = tag

        self._hash = None

        weak_references = type(self)._weak_references_by_object_id.get(id(object), None)

        if weak_references is None:
            weak_references = _WeakSet(self._on_weak_set_empty, id(object))
            weak_references.add(self)
            type(self)._weak_references_by_object_id[id(object)] = weak_references

        else:
            weak_references.add(self)

    def _on_weak_set_empty(self, weak_set):
        del self.weak_references[weak_set.object_id]

    def collect(self):
        if self.object is COLLECTED:  # already collected
            return True

        weak_references = type(self)._weak_references_by_object_id[id(self.object)]

        if sys.getrefcount(self.object) != len(weak_references) + 1:  # still referenced
            return False

        del type(self)._weak_references_by_object_id[id(self.object)]

        for weak_reference in weak_references:
            weak_reference.callback(weak_reference)
            weak_reference.object = COLLECTED

    def __hash__(self):
        result = self._hash
        if result is not None:
            return result

        result = hash(id(self)) if self.object is COLLECTED else hash(self.object)
        self._hash = result
        return result

    def __eq__(self, other):
        if self.object is COLLECTED:
            raise Exception("object has been collected already")

        if not isinstance(other, type(self)):
            result = False
            return result

        if other.object is COLLECTED:
            raise Exception("object has been collected already")

        result = other.object == self.object
        return result


class _WeakSet(c_abc.MutableSet):
    def __init__(self, empty_callback, object_id):
        super().__init__()

        self.empty_callback = empty_callback
        self.object_id = object_id

        self._set = set()

    def __contains__(self, item):
        raise Exception("shouldn't happen")

    def __iter__(self):
        result = iter(weak_reference() for weak_reference in self._set)
        return result

    def __len__(self):
        result = len(self._set)
        return result

    def add(self, item):
        self._set.add(_ref(item, self._callback))

    def _callback(self, weak_reference):
        self._set.remove(weak_reference)

        if len(self) == 0:
            self.empty_callback(self)

    def discard(self, item):
        raise Exception("shouldn't happen")


class _ref(weakref.ref):
    def __hash__(self):
        result = hash(id(self))
        return result

    def __eq__(self, other):
        result = other is self
        return result


class WeakSet(c_abc.MutableSet):
    def __init__(self, items=None):
        super().__init__()

        self._set = set()

        if items is not None:
            self.update(items)

    def __contains__(self, item):
        result = WeakReference(item, None) in self._set
        return result

    def __iter__(self):
        result = (
            weak_reference.object
            for weak_reference in self._set
            if weak_reference.object is not COLLECTED
        )
        return result

    def __len__(self):
        result = len(self._set)
        return result

    def add(self, item, callback=None, tag=None):
        self._set.add(
            WeakReference(item, self._pass_1 if callback is None else callback, tag=tag)
        )

    def _pass_1(self, _):
        pass

    def discard(self, item):
        self._set.discard(WeakReference(item, None))

    def collect(self):
        collected_references = set()

        for weak_reference in self._set:
            if weak_reference.collect():
                collected_references.add(weak_reference)

        self._set.difference_update(collected_references)


class WeakKeyDictionary(c_abc.MutableMapping):
    def __init__(self, items=None):
        super().__init__()

        self._dict = {}

        if items is not None:
            self.update(items)

    def __getitem__(self, key):
        result = self._dict[WeakReference(key, None)]
        return result

    def __setitem__(self, key, value):
        self.set_item(key, value, self._pass_1)

    def _pass_1(self, _):
        pass

    def set_item(self, key, value, callback, tag=None):
        self._dict[WeakReference(key, callback, tag=tag)] = value

    def __delitem__(self, key):
        del self._dict[WeakReference(key, None)]

    def __iter__(self):
        result = (
            weak_reference.object
            for weak_reference in self._dict.keys()
            if weak_reference.object is not COLLECTED
        )
        return result

    def __len__(self):
        result = len(self._dict)
        return result

    def collect(self):
        collected_references = []

        for weak_reference in self._dict.keys():
            if weak_reference.collect():
                collected_references.append(weak_reference)

        for weak_reference in collected_references:
            del self._dict[weak_reference]


class WeakValueDictionary(c_abc.MutableMapping):
    def __init__(self, items=None):
        super().__init__()

        self._dict = {}

        if items is not None:
            self.update(items)

    def __getitem__(self, key):
        result = self._dict[key].object

        if result is COLLECTED:
            raise KeyError(key)

        return result

    def __setitem__(self, key, value):
        self.set_item(key, value, self._pass_1)

    def _pass_1(self, _):
        pass

    def set_item(self, key, value, callback, tag=None):
        self._dict[key] = WeakReference(value, callback, tag=tag)

    def __delitem__(self, key):
        del self._dict[key]

    def __iter__(self):
        result = (
            key
            for key, weak_reference in self._dict.items()
            if weak_reference.object is not COLLECTED
        )
        return result

    def __len__(self):
        result = len(self._dict)
        return result

    def collect(self):
        collected_keys = []

        for key, weak_reference in self._dict.items():
            if weak_reference.collect():
                collected_keys.append(key)

        for key in collected_keys:
            del self._dict[key]
