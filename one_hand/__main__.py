"""
- features
  - lookup
    - conditions
      - a..z key is down or held (and no modifier keys are down or held)
      - or Shift key is down or held (and no other modifiers are down or held)
        - and a..z key is down or held
    - consequences
      - get string for key
      - append string to input string
      - lookup input string in specialized dictionary
      - if no previous lookup
        - emit key events which write current best match
      - if previous lookup
        - emit key events which delete previous match
        - emit key events which write current best match
  - next
    - conditions
      - no modifiers are down or held
      - and next key is down
    - consequences
      - if input not committed
        - emit key events which write a space
        - set input committed
      - if input committed
        - emit key events which delete current match
        - emit key events which write next match
  - previous
    - conditions
      - Shift key is down or held (and no other modifiers are down or held)
      - and next key is down or held
    - consequences
      - if input committed
        - emit key events which delete current match
        - emit key events which write previous match
      - if input not committed
        - if next key is space key
          - append space to input string
          - perform lookup
  - alternative next
    - conditions
      - no modifier keys are down or held
      - and next key is held
    - consequences
      - if previously not held
        - undo next
        - interrupt input with a newline (KEY_ENTER; special type of commit)
  - back
    - conditions
      - no modifier keys are down or held
      - and back key is down or held
    - consequences
      - if match
        - emit key events which delete current match
      - if no match
        - emit key events which write backspace
  - alternative back
    - conditions
      - Shift key is down or held (and no other modifiers are down or held)
      - and back key is down or held
    - consequences
      - if match
        - remove last string from input string
        - perform lookup
      -  if no match
        - emit key events which write backspace
  - pause
    - conditions
      - no modifier keys are down or held
      - and pause key is down
    - consequences
      - if not pause
        - pause processing of key events
        - start passing through key events
      - if pause
        - resume processing of key events
        - stop passing through key events
  - exit
    - conditions
      - no modifier keys are down or held
      - and pause key is held
    - consequences
      - exit
  - other key events
    - conditions
      - any modifier key is down or held
      - and any other key is down or held
    - consequences
      - interrupt input with empty string
      - emit key events
  - non-key events
    - conditions
      - non-key event received
    - consequences
      - pass through event
"""

import one_hand
import one_hand.evdev as o_evdev
import one_hand.process_tree as o_process_tree
import evdev
import evdev.ecodes as e_ecodes
import argparse
import itertools
import logging
import os.path as o_path
import re
import tempfile


# definitions

NEXT_KEY_CODE = e_ecodes.KEY_SPACE
BACK_KEY_CODES = [e_ecodes.KEY_CAPSLOCK, e_ecodes.KEY_APOSTROPHE, e_ecodes.KEY_ENTER]
PAUSE_KEY_CODES = [e_ecodes.KEY_F3, e_ecodes.KEY_F10]

MODIFIER_MASK_BY_KEY_CODE = {
    e_ecodes.KEY_LEFTSHIFT: o_evdev.SHIFT_MASK,
    e_ecodes.KEY_RIGHTSHIFT: o_evdev.SHIFT_MASK,
    e_ecodes.KEY_RIGHTALT: o_evdev.RIGHT_ALT_MASK,
    e_ecodes.KEY_LEFTCTRL: o_evdev.CONTROL_MASK,
    e_ecodes.KEY_RIGHTCTRL: o_evdev.CONTROL_MASK,
    e_ecodes.KEY_LEFTALT: o_evdev.LEFT_ALT_MASK,
}

WHITELISTED_KEYS = [
    (frozenset(modifier_masks), e_ecodes.ecodes["KEY_{}".format(name)])
    for modifier_masks in [set(), {o_evdev.SHIFT_MASK}]
    for name in itertools.chain("QWERTASDFGZXCVB", "YUIOPHJKLNM", ["SEMICOLON"])
]

#


KEY_CODE_BY_MODIFIER_MASK = {
    modifier_mask: key_code
    for key_code, modifier_mask in MODIFIER_MASK_BY_KEY_CODE.items()
}


# main


class Main:
    """
    - idea
      - on new string (character)
        - update current word
        - lookup dictionary
        - update output to show best match
          - remove old best match by backspace
          - write new best match
      - on space
        - if current word has no parts
          - update output to show next best match of previous word
        - else
          - start new word
      - on shift space
        - if current word has no parts
          - if previous best match of previous word exists
            - update output to show
        - else
          - trigger on new part with space
      - on backspace
        - if current word has no parts
          - remove current word
          - return to previous word
        - else
          - remove all parts
    """

    def __init__(self, suggester, key_by_string, u_input):
        self.suggester = suggester
        self.key_by_string = key_by_string
        self.u_input = u_input

        self._input = None  # str

        self._suggestions = None  # sequence
        self._suggestion_index = None  # int

        self._output = None  # str

        self.reset()

    def reset(self):
        self._input = ""
        self._suggestions = []
        self._suggestion_index = 0
        self._output = ""

    def add_string(self, string):
        if len(self._input) != 0:
            o_evdev.delete_characters(len(self._output), self.u_input)

        self._input += string

        self._suggestions = one_hand.LazySequence(
            self.suggester.get_suggestions(self._input)
        )
        self._suggestion_index = 0

        try:
            suggestion = self._suggestions[self._suggestion_index]

        except IndexError:
            suggestion = self._input

        o_evdev.write_string(
            suggestion, KEY_CODE_BY_MODIFIER_MASK, self.key_by_string, self.u_input
        )
        self._output = suggestion

    def handle_next(self):
        if len(self._input) == 0:
            result = self._update_suggestion(self._suggestion_index + 1)
            return result

        else:
            self._input = ""

            o_evdev.write_string(
                " ", KEY_CODE_BY_MODIFIER_MASK, self.key_by_string, self.u_input
            )
            self._output += " "

            return False

    def handle_previous(self):
        if len(self._input) == 0:
            self._update_suggestion(self._suggestion_index - 1)

        else:
            if NEXT_KEY_CODE == e_ecodes.KEY_SPACE:
                self.add_string(" ")

    def _update_suggestion(self, index):
        # get suffix
        try:
            suggestion = self._suggestions[self._suggestion_index]

        except IndexError:
            suffix = ""

        else:
            suffix = self._output[len(suggestion) :]
        #

        if (
            re.search(r"[ \t\n\r]$", suffix) is None
        ):  # see self.interrupt and self.handle_alternative_back
            o_evdev.write_string(
                " ", KEY_CODE_BY_MODIFIER_MASK, self.key_by_string, self.u_input
            )
            self._output += " "
            return False

        try:
            suggestion = self._suggestions[index]

        except IndexError:
            return False

        self._suggestion_index = index

        o_evdev.delete_characters(len(self._output), self.u_input)

        output = suggestion + suffix
        o_evdev.write_string(
            output, KEY_CODE_BY_MODIFIER_MASK, self.key_by_string, self.u_input
        )
        self._output = output

        return True

    def handle_back(self):
        if len(self._output) == 0:
            o_evdev.delete_characters(1, self.u_input)

        else:
            o_evdev.delete_characters(len(self._output), self.u_input)
            self.reset()

    def handle_alternative_back(self):
        if len(self._input) == 0:
            o_evdev.delete_characters(1, self.u_input)
            self._output = self._output[:-1]

        else:
            self._input = self._input[:-1]
            self.add_string("")

    def interrupt(self, string):
        self._input = ""

        if len(string) == 0:
            self.reset()

        else:
            o_evdev.write_string(
                string, KEY_CODE_BY_MODIFIER_MASK, self.key_by_string, self.u_input
            )
            self._output += string


# event processing


class ProcessNode(o_process_tree.Node):
    def _process(self, version, event):
        raise NotImplementedError


class NotKeyProcessNode(ProcessNode):
    def __init__(self, u_input):
        super().__init__()

        self.u_input = u_input

        self.is_not_key = None

    def _process(self, version, event):
        if event.type == e_ecodes.EV_KEY:
            self.is_not_key = False
            return

        self.u_input.write_event(event)
        self.is_not_key = True


class ModifierProcessNode(ProcessNode):
    def __init__(self, not_key_node):
        super().__init__()

        self.not_key_node = not_key_node

        self.modifier_masks = set()
        self.processed = None

    def _process(self, version, event):
        return_value = o_process_tree.process(version, [self.not_key_node])
        if o_process_tree.is_exit_or_stop(return_value):
            return return_value

        if self.not_key_node.is_not_key:
            self.processed = False
            return

        modifier_mask = MODIFIER_MASK_BY_KEY_CODE.get(event.code, None)

        if modifier_mask is None:
            self.procesed = False
            return

        if event.value == o_evdev.KEY_UP:
            self.modifier_masks.remove(modifier_mask)

        else:
            self.modifier_masks.add(modifier_mask)

        self.processed = True


class PauseProcessNode(ProcessNode):
    def __init__(self, not_key_node, modifier_node):
        super().__init__()

        self.not_key_node = not_key_node
        self.modifier_node = modifier_node

        self.paused = False
        self.processed = None

        self._hold = None

    def _process(self, version, event):
        return_value = o_process_tree.process(
            version, [self.not_key_node, self.modifier_node]
        )
        if o_process_tree.is_exit_or_stop(return_value):
            return return_value

        if (
            self.not_key_node.is_not_key
            or event.code not in PAUSE_KEY_CODES
            or self.modifier_node.modifier_masks != set()
        ):
            self.processed = False
            return

        if event.value != o_evdev.KEY_UP:
            self._hold = event.value == o_evdev.KEY_HOLD
            self.processed = True
            return

        if self._hold:
            result = o_process_tree.ReturnValue(exit=True)
            return result

        else:
            self.paused = not self.paused
            self.processed = True


class PassProcessNode(ProcessNode):
    def __init__(self, not_key_node, pause_node, u_input):
        super().__init__()

        self.not_key_node = not_key_node
        self.pause_node = pause_node
        self.u_input = u_input

        self.passed = None

    def _process(self, version, event):
        return_value = o_process_tree.process(
            version, [self.not_key_node, self.pause_node]
        )
        if o_process_tree.is_exit_or_stop(return_value):
            return return_value

        if self.pause_node.processed:
            self.passed = True  # pretend passed
            return

        if self.not_key_node.is_not_key or not self.pause_node.paused:
            self.passed = False
            return

        self.u_input.write_event(event)
        self.passed = True


class NextKeyProcessNode(ProcessNode):
    def __init__(self, not_key_node, pass_node, modifier_node, main):
        super().__init__()

        self.not_key_node = not_key_node
        self.pass_node = pass_node
        self.modifier_node = modifier_node
        self.main = main

        self.processed = None

        self._next_suggested = None
        self._hold_processed = False

    def _process(self, version, event):
        return_value = o_process_tree.process(
            version, [self.not_key_node, self.pass_node, self.modifier_node]
        )
        if o_process_tree.is_exit_or_stop(return_value):
            return return_value

        if (
            self.not_key_node.is_not_key
            or pass_node.passed
            or event.code != NEXT_KEY_CODE
            or self.modifier_node.modifier_masks not in [set(), {o_evdev.SHIFT_MASK}]
        ):
            self.processed = False
            return

        if event.value == o_evdev.KEY_UP:
            self.processed = False
            self._hold_processed = False
            return

        if o_evdev.SHIFT_MASK in self.modifier_node.modifier_masks:
            self.main.handle_previous()

        else:
            if event.value == o_evdev.KEY_DOWN:
                self._next_suggested = self.main.handle_next()

            elif not self._hold_processed:
                if self._next_suggested:
                    self.main.handle_previous()

                self.main.handle_alternative_back()
                self.main.interrupt("\r")

                self._hold_processed = True

        self.processed = True


class BackKeyProcessNode(ProcessNode):
    def __init__(self, not_key_node, pass_node, modifier_node, main):
        super().__init__()

        self.not_key_node = not_key_node
        self.pass_node = pass_node
        self.modifier_node = modifier_node
        self.main = main

        self.processed = None

    def _process(self, version, event):
        return_value = o_process_tree.process(
            version, [self.not_key_node, self.pass_node, self.modifier_node]
        )
        if o_process_tree.is_exit_or_stop(return_value):
            return return_value

        if (
            self.not_key_node.is_not_key
            or self.pass_node.passed
            or event.code not in BACK_KEY_CODES
            or event.value == o_evdev.KEY_UP
            or self.modifier_node.modifier_masks not in [set(), {o_evdev.SHIFT_MASK}]
        ):
            self.processed = False
            return

        if o_evdev.SHIFT_MASK in self.modifier_node.modifier_masks:
            self.main.handle_alternative_back()

        else:
            self.main.handle_back()

        self.processed = True


class StringProcessNode(ProcessNode):
    def __init__(
        self,
        pause_node,
        not_key_node,
        pass_node,
        modifier_node,
        next_key_node,
        back_key_node,
        string_by_key,
        main,
        u_input,
    ):
        super().__init__()

        self.pause_node = pause_node
        self.not_key_node = not_key_node
        self.pass_node = pass_node
        self.modifier_node = modifier_node
        self.next_key_node = next_key_node
        self.back_key_node = back_key_node
        self.string_by_key = string_by_key
        self.main = main
        self.u_input = u_input

    def _process(self, version, event):
        return_value = o_process_tree.process(
            version,
            [
                self.pause_node,
                self.not_key_node,
                self.pass_node,
                self.modifier_node,
                self.next_key_node,
            ],
        )
        if o_process_tree.is_exit_or_stop(return_value):
            return return_value

        if self.pause_node.processed:
            self.main.reset()
            return

        if (
            self.not_key_node.is_not_key
            or self.pass_node.passed
            or self.modifier_node.processed
            or self.next_key_node.processed
            or self.back_key_node.processed
            or event.value == o_evdev.KEY_UP
        ):
            return

        key = (frozenset(self.modifier_node.modifier_masks), event.code)
        string = self.string_by_key.get(key, None)

        if key not in WHITELISTED_KEYS:
            if string is None:
                self.main.interrupt("")
                o_evdev.write_key(key, KEY_CODE_BY_MODIFIER_MASK, self.u_input)

            else:
                self.main.interrupt(string)

            return

        self.main.add_string("" if string is None else string)


logging.basicConfig(level="INFO", format="%(asctime)s %(levelname)s %(message)s")


# parse arguments

parser = argparse.ArgumentParser()

parser.add_argument("dict", help="Path to dictionary")
parser.add_argument("dev", help="Path to keyboard device")

arguments = parser.parse_args()

#

logging.info("preparing")

# prepare dictionary
with one_hand.evdev.X11() as x11:
    character_mapping = one_hand.get_character_mapping_from_x11(x11)

reverse_character_mapping = {
    to_character: from_character
    for from_character, to_character in character_mapping.items()
}

with tempfile.TemporaryDirectory() as working_path:
    dictionary_path = o_path.join(working_path, "dictionary")

    word_mapping = one_hand.PlyvelDict(o_path.join(working_path, "mapping"))

    one_hand.prepare_symspell_dictionary(
        arguments.dict,
        [character_mapping, reverse_character_mapping],
        dictionary_path,
        word_mapping,
    )

    for from_word, to_words in word_mapping.items():
        word_mapping[from_word] = tuple(to_words)

    word_mapping.collect()
    #

    input_device = evdev.InputDevice(arguments.dev)
    u_input = evdev.UInput()

    # get string by key

    with o_evdev.X11() as x11:
        string_by_key = {
            (frozenset(shift | right_alt), key_code): string
            for shift in [set(), {o_evdev.SHIFT_MASK}]
            for right_alt in [set(), {o_evdev.RIGHT_ALT_MASK}]
            for key_code, string in x11.get_strings_by_key_codes(
                modifier_masks=shift | right_alt
            ).items()
        }

    # main

    def key_code(item):
        (_, result), _ = item
        return result

    key_by_string = {
        string: key
        for key, string in sorted(string_by_key.items(), key=key_code, reverse=True)
    }

    words_dict = one_hand.PlyvelDict(o_path.join(working_path, "words"))
    deletes_defaultdict = one_hand.PlyvelDefaultDict(
        list, o_path.join(working_path, "deletes")
    )

    suggester = one_hand.Suggester(
        dictionary_path,
        word_mapping,
        words_dict=words_dict,
        deletes_defaultdict=deletes_defaultdict,
    )

    for key, list_ in deletes_defaultdict.items():
        deletes_defaultdict[key] = tuple(list_)

    words_dict.collect()
    deletes_defaultdict.collect()

    main = Main(suggester, key_by_string, u_input)

    # event processing

    process_nodes = []

    not_key_node = NotKeyProcessNode(u_input)
    process_nodes.append(not_key_node)

    modifier_node = ModifierProcessNode(not_key_node)
    process_nodes.append(modifier_node)

    pause_node = PauseProcessNode(not_key_node, modifier_node)
    process_nodes.append(pause_node)

    pass_node = PassProcessNode(not_key_node, pause_node, u_input)
    process_nodes.append(pass_node)

    next_key_node = NextKeyProcessNode(not_key_node, pass_node, modifier_node, main)
    process_nodes.append(next_key_node)

    back_key_node = BackKeyProcessNode(not_key_node, pass_node, modifier_node, main)
    process_nodes.append(back_key_node)

    string_node = StringProcessNode(
        pause_node,
        not_key_node,
        pass_node,
        modifier_node,
        next_key_node,
        back_key_node,
        string_by_key,
        main,
        u_input,
    )
    process_nodes.append(string_node)

    #

    logging.info("ready")

    with input_device.grab_context():
        for version, event in enumerate(input_device.read_loop()):
            return_value = o_process_tree.process(version, process_nodes, event)
            if return_value is not None and return_value.exit is True:
                break

    words_dict.close()
    deletes_defaultdict.close()

    word_mapping.close()
